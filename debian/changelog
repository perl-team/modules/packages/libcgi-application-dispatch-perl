libcgi-application-dispatch-perl (3.12-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 22:56:48 +0100

libcgi-application-dispatch-perl (3.12-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 13:37:14 +0100

libcgi-application-dispatch-perl (3.12-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Make the test suite work with Apache 2.4
  * Make the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Sun, 07 Jun 2015 23:34:31 +0300

libcgi-application-dispatch-perl (3.12-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Xavier Guimard ]
  * New upstream release
  * Bump Standards-Version to 3.9.4
  * Use debhelper 8
  * Update debian/copyright to format 1.0
  * Remove deprecation.patch now included in upstream
  * Remove obsolete lintian-overrides

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 04 Nov 2012 07:52:53 +0100

libcgi-application-dispatch-perl (3.07-2) unstable; urgency=low

  [ gregor herrmann ]
  * Remove version from libversion-perl (build) dependency.

  [ Nicholas Bamber ]
  * Added patch fixing 5.14 deprecation of qw(..) without enclosing
    parentheses where those parentheses are part of statement syntax
  * Added lintian override concerning DEP-5

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 26 Nov 2011 20:09:12 +0000

libcgi-application-dispatch-perl (3.07-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nicholas Bamber ]
  * New upstream release
  * Removed dependency on Exception::Class::TryCatch

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 09 Sep 2011 22:13:22 +0100

libcgi-application-dispatch-perl (3.04-1) unstable; urgency=low

  * Split out from libcgi-application-basic-plugin-bundle-perl (Closes: #630878)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Wed, 29 Jun 2011 17:36:24 +0100
